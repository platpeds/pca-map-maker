import matplotlib as mpl
import matplotlib.pyplot as plt
 
def show_colors(colors):
    """
    Draw a square for each color contained in the colors list
    given in argument.
    """
    with plt.rc_context(plt.rcParamsDefault):
        fig = plt.figure(figsize=(12, 1), frameon=False)
        ax = fig.add_subplot(111)
        for x, color in enumerate(colors):
            ax.add_patch(
                mpl.patches.Rectangle(
                    (x, 0), 2, 1, facecolor=color
                )
            )
        ax.set_xlim((0, len(colors)))
        ax.set_ylim((0, 1))
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_aspect("equal")
 
    return fig


colors = ['#000000',
 '#225ea8',
'#1d91c0',
'#41b6c4',
'#7fcdbb',
'#c7e9b4',
'#ffffcc']
fig = show_colors(colors)
plt.show()