from selenium import webdriver
from selenium import common
from datetime import datetime
import csv
import re
import math
import linecache
from selenium.common import exceptions
from selenium.webdriver.common.alert import Alert
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementNotVisibleException
from selenium.common.exceptions import ElementNotSelectableException, StaleElementReferenceException, NoSuchFrameException, UnexpectedAlertPresentException, NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.select import By
from selenium.webdriver.common.service import time
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common import keys, by
from selenium.webdriver.common.keys import Keys
from time import sleep
from bs4 import BeautifulSoup
import pandas as pd
from pandas import ExcelWriter
import numpy
#import cairo
from collections import Counter

if True:  #populate the map based only on presbytery boundaries
  colorInfo = ''
  scored_element = 'ratio'
  allData = pd.read_excel('C:/Users/Suzanne/Box/PCA Maps/PCA County map 2018.xlsx',sheet_name='PresbyTable',converters={'FIPS':str})
  colorList = pd.read_excel('C:/Users/Suzanne/Box/PCA Maps/PCA County map 2018.xlsx',sheet_name='PresbyteryColors')
  rawStatefile = open('C:/Users/Suzanne/Box/PCA Maps/FIPS Counties.svg',"r")
  rawStates = rawStatefile.read()#.decode('utf-8','ignore')
  pathSet = rawStates.split("<path")
  outputString = rawStates[0]
  print(outputString)
  for j in range(1,len(pathSet)):  #iterating through all the FIPS in the SVG
     thisPath = pathSet[j]
     print(thisPath)
     idThis = thisPath.find("id=")
     thisID = thisPath[idThis+4:idThis+9]
     print(thisID)
     rowMatch = allData[(allData['FIPS'] == thisID)]  #finds the county with that FIPS in Presbytable
     if (len(rowMatch) > 0):
         rowMatch.index = range(len(rowMatch))
         myPres =str( rowMatch.loc[0,'Presbytery'])  #finds the corresponding presbytery for this county
         colorRows = colorList[colorList['presbytery']==myPres]   #find the matching color row for the presbytery in PresbyColors
         colorRows.index = range(len(colorRows.index))
         if len(colorRows.index) >0:
              myColor = colorRows.loc[0,'hue']
         else:
              myColor = '#d0d0d0'
         #print(myColor)
         thisPath = thisPath.replace("fill:#d0d0d0","fill:"+myColor)
        # print(thisPath)
     outputString += "<path" + thisPath
  legend = ''
  use_legend = False
  if use_legend == True:
    for i,row in colorList.iterrows():
       x = str(row['x']-15); y = str(row['y']+7); abbrev = row['abbrev']
       legend = legend + '<text x="' + str(x) + '" y="' + str(y) +  '" font-family="sans-serif" font-size="7px" fill="black">' + abbrev + '</text>'
  text_file = open('C:/Users/suzanne/Box/PCA Maps/Presbyteries2019nolabels.svg',"w")
  header = open('C:/Users/suzanne/Box/PCA Maps/FIPSHeader.txt','r')
  h = header.read()
  text_file.write(h)
  text_file.write(outputString[1:-7])
  text_file.write(legend)
  text_file.write('</svg>')
  text_file.close() 
  header.close()

myColors =  ['#ffffb2','#ccebc5','#a8ddb5','#7bccc4','#4eb3d3','#2b8cbe','#08589e']
LI = 'ABCDEFG'
if False:  #populate the map based some other value
  colorInfo = ''
  scored_element = 'ratio'
  separating_element = 'grouping'
  allData = pd.read_excel('C:/Users/Suzanne/Box/PCA Maps/PCA County map.xlsx',sheet_name='PresbyTable',converters={'FIPS':str})
  colorData = pd.read_excel('C:/Users/Suzanne/Box/PCA Maps/Commissioners_2018.xlsx',sheet_name='TERE')
 
  rawStatefile = open('C:/Users/Suzanne/Box/PCA Maps/FIPS Counties.svg',"r")
  rawStates = rawStatefile.read()#.decode('utf-8','ignore')
  pathSet = rawStates.split("<path")
  outputString = rawStates[0]
  print(outputString)
  for j in range(1,len(pathSet)):  #iterating through all the FIPS in the SVG
     thisPath = pathSet[j]
     print(thisPath)
     idThis = thisPath.find("id=")
     thisID = thisPath[idThis+4:idThis+9]
     print(thisID)
     rowMatch = allData[(allData['FIPS'] == thisID)]  #finds the county with that FIPS in Presbytable
     if (len(rowMatch) > 0):
         rowMatch.index = range(len(rowMatch))
         myPres =str( rowMatch.loc[0,'Presbytery'])  #finds the corresponding presbytery for this county
         if myPres in ('disputed', 'split'):  #put in a whole substitute where needed
              myPres =str( rowMatch.loc[0,'if_split'])  
         colorRows = colorData[colorData['presbytery']==myPres]   #find the matching color row for the presbytery in PresbyColors
         colorRows.index = range(len(colorRows.index))
         if len(colorRows.index) >0:
              myColorGrouping = str(colorRows.loc[0,'grouping'])
              i = LI.find(myColorGrouping)
              myColor = myColors[i]
         else:
              myColor = '#525252'
         #print(myColor)
         thisPath = thisPath.replace("fill:#d0d0d0","fill:"+myColor)
        # print(thisPath)
     outputString += "<path" + thisPath
  legend = ''
 # for i,row in colorList.iterrows():
 #     x = str(row['x']-15); y = str(row['y']+7); abbrev = row['abbrev']
 #     legend = legend + '<text x="' + str(x) + '" y="' + str(y) +  '" font-family="sans-serif" font-size="7px" fill="black">' + abbrev + '</text>'
  text_file = open('C:/Users/suzanne/Box/PCA Maps/Presbyteries_with_RE_2018.svg',"w")
  header = open('C:/Users/suzanne/Box/PCA Maps/FIPSHeader.txt','r')
  h = header.read()
  text_file.write(h)
  text_file.write(outputString[1:-7])
  text_file.write(legend)
  text_file.write('</svg>')
  text_file.close() 
  header.close()
