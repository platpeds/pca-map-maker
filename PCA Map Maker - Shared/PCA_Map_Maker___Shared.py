from selenium import webdriver
from selenium import common
from datetime import datetime
import csv
import re
import math
import linecache
from selenium.common import exceptions
from selenium.webdriver.common.alert import Alert
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementNotVisibleException
from selenium.common.exceptions import ElementNotSelectableException, StaleElementReferenceException, NoSuchFrameException, UnexpectedAlertPresentException, NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.select import By
from selenium.webdriver.common.service import time
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common import keys, by
from selenium.webdriver.common.keys import Keys
from time import sleep
from bs4 import BeautifulSoup
import pandas as pd
from pandas import ExcelWriter
import numpy
import cairo
from collections import Counter


partTwo = True
partZ = False


ChromePath = 'C:/Source/NCIR/NCIR/static/chromedriver.exe'

colors = ['#FFCCFF',
 

'#F2B3F9',
 

'#E699F2',
 

'#D980EC',
 

'#CC66E6',

'#BF4DDF',

'#B333D9',

'#A61AD2',

'#9900CC']
 
yellowreds = ['#ffffcc','#ffeda0','#fed976','#feb24c','#fd8d3c','#fc4e2a','#e31a1c','#bd0026','#800026']
bluegreens = ['#f7fcf0','#e0f3db','#ccebc5','#a8ddb5','#7bccc4','#4eb3d3','#2b8cbe','#0868ac','#084081']
purples = ['#fcfbfd','#efedf5','#dadaeb','#bcbddc','#9e9ac8','#807dba','#6a51a3','#54278f','#3f007d']

colors = yellowreds
colors = list(reversed(colors))
 
xpath2 = '//input[@name="submit"]'

states_list=[

'AK', 
'AL', 
'AR', 
'AZ', 
'CA', 
'CO', 
'CT', 
'DC', 
'DE', 
'FL', 
'GA', 
'HI', 
'IA', 
'ID', 
'IL', 
'IN', 
'KS', 
'KY', 
'LA', 
'MA', 
'MD', 
'ME', 
'MI','MO', 'MN','MS','MT', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VA', 'VT', 'WA', 'WI', 'WV', 'WY']
PCA_Churches = []

partC = False

if (partC == True):  #extractzipcodes
   print("starting...")
   rawSOAPM = 'C:/VSHPScraper/SOAPM.xlsx'
   zips = []
   print("more")
   rawS = pd.read_excel(rawSOAPM)
   print("hi")
   print(rawS)
   regex = r"[A-Z]{2}\s{1,4}\d{5}"
   print("here")
   for i in xrange(0,len(rawS.index)):
      print(i)
      wholeAddress = str(rawS.loc[i,'complete_address'])
      print(wholeAddress)
      matches = re.search(regex, wholeAddress,re.M|re.I)
    #  print(matches)
      if matches:
         a = matches.group()
         b = a[len(a)-5:]
         print(b)
         zips.append(b)
      else:
         zips.append('AAAAA')
   rawS['zip'] = zips
   rawS.to_excel('C:/VSHPScraper/SOAPMZips.xlsx',index=False)
   print("Written")

if (partZ == True):
   zipData = pd.read_excel('C:/VSHPScraper/Zips for Lookup.xlsx')
   driver = webdriver.Chrome(ChromePath)
   webpage = 'https://www.zipinfo.com/search/zipcode.htm'
   driver.get(webpage)
   xpathcheck = '//input[@name="cnty"]'
   driver.find_element_by_xpath(xpathcheck).click()
   xpathzip = '//input[@name="zip"]'
   xpathgo = '//input[@name="Go"]'
   fips=[]
   for i in xrange(0,len(zipData.index)):
       thisZip = str(zipData.loc[i,'zips']).zfill(5)
       driver.find_element_by_xpath(xpathzip).send_keys(thisZip)
       driver.find_element_by_xpath(xpathgo).click()
       html = driver.page_source
       startHere = html.find("</th></tr>")
       endHere = html.find("</tr>",startHere+8)
       bitTo = (html[startHere:endHere])
       fip = (bitTo[len(bitTo)-10:len(bitTo)-5])
       print(fip)
       fips.append(fip)
       driver.back()
       driver.find_element_by_xpath(xpathzip).send_keys(Keys.CONTROL+'a')
       driver.find_element_by_xpath(xpathzip).send_keys(Keys.DELETE)
   zipData['fips'] = fips
   zipData.to_excel('C:/VSHPScraper/FipsFound.xlsx') # zip to FIPS lookups

if (partTwo == True):   #populate the map
  colorInfo = ''
  scored_element = 'ratio'
  allData = pd.read_excel('C:/VSHPScraper/SOAPMZips.xlsx',sheetname='Sheet4',converters={'FIPS':str})
  rawStatefile = open('C:/Users/Suzanne/Desktop/Box Sync/PCA Maps/FIPS Counties.svg',"r")
  rawStates = rawStatefile.read()
  pathSet = rawStates.split("<path")
  outputString = rawStates[0]
  print(outputString)
  for j in xrange(1,len(pathSet)):
     thisPath = pathSet[j]
     print(thisPath)
     idThis = thisPath.find("id=")
     thisID = thisPath[idThis+4:idThis+9]
     print(thisID)
     rowMatch = allData[(allData['FIPS'] == thisID)]
     if (len(rowMatch) > 0):
         rowMatch.index = range(len(rowMatch))
         SOAPMers =str( rowMatch.loc[0,'numcolor'])
         if SOAPMers == 'AA':
           myColor = colors[8]
         if SOAPMers == 'A':
           myColor = colors[7]
         elif SOAPMers == 'B':
           myColor = colors[6]
         elif SOAPMers == 'C':
           myColor = colors[5]
         elif SOAPMers == 'D':
           myColor = colors[4]
         elif SOAPMers == 'E':
           myColor = colors[3]
         elif SOAPMers == 'F':
           myColor = colors[2]
         elif SOAPMers == 'G':
           myColor = colors[1]
         elif SOAPMers == 'H':
           myColor = colors[0]

       #  else:
        #   myColor = colors[8]

         #print(myColor)
         thisPath = thisPath.replace("fill:#d0d0d0","fill:"+myColor)
        # print(thisPath)
     outputString += "<path" + thisPath

  text_file = open('C:/VSHPScraper/NumPedsSOAPM.svg',"w")
  header = open('C:/VSHPScraper/FIPSHeader.txt','r')
  h = header.read()
  text_file.write(h)
  text_file.write(outputString[1:])
  text_file.close() 
  header.close()
