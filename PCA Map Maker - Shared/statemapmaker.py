
from selenium import webdriver
from selenium import common
from datetime import datetime
import csv
import re
import math
import linecache
from selenium.common import exceptions
from selenium.webdriver.common.alert import Alert
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementNotVisibleException
from selenium.common.exceptions import ElementNotSelectableException, StaleElementReferenceException, NoSuchFrameException, UnexpectedAlertPresentException, NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.select import By
from selenium.webdriver.common.service import time
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common import keys, by
from selenium.webdriver.common.keys import Keys
from time import sleep

import pandas as pd
from pandas import ExcelWriter
import numpy as np
from scipy import stats


greens = ['#edf8fb','#ccece6','#99d8c9','#66c2a4','#41ae76','#238b45','#005824']
pinks = ['#f1eef6','#d4b9da','#c994c7','#df65b0','#e7298a','#ce1256','#91003f']
colorguide = {'zero':'#000000',
'<1': '#225ea8',
'1-2':'#1d91c0',
'2-3':'#41b6c4',
'3-4':'#7fcdbb',
'4-5':'#c7e9b4',
'5+':'#ffffcc'
}



if True:  

  allData = pd.read_excel('C:/atelier/soapm_state.xlsx')
  rawStatefile = open('C:/Users/Suzanne/Box/PCA Maps/States Plain SVG.txt',"r")
  rawStates = rawStatefile.read().decode('utf-8','ignore')
  pathSet = rawStates.split("<path")
  outputString = rawStates[0]
  print(outputString)
  for j in xrange(1,len(pathSet)):  #iterating through all the FIPS in the SVG
     thisPath = pathSet[j]
     print(thisPath)
     idThis = thisPath.find("id=")
     thisID = thisPath[idThis+4:idThis+6]
     print(thisID)
     rowMatch = allData[(allData['state'] == thisID)]  #finds the county with that FIPS in imported data
     if (len(rowMatch) > 0):
         rowMatch.index = range(len(rowMatch))
         peds =str( rowMatch.loc[0,'categ'])
         col = colorguide[peds]
         thisPath = thisPath.replace("fill:#aaaaaa","fill:"+col)

     outputString += "<path" + thisPath
  legend = ''
  text_file = open('C:/atelier/RVU_by_state.svg',"w")
  header = open('C:/atelier/StateMapHeader.txt','r')
  h = header.read()
  text_file.write(h)
  text_file.write(outputString[1:-7])

  text_file.write('</svg>')
  text_file.close() 
  header.close()

