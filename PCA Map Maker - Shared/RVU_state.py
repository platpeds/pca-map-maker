from __future__ import division
from selenium import webdriver
from selenium import common
from datetime import datetime
import csv
import re
import math
import linecache
from selenium.common import exceptions
from selenium.webdriver.common.alert import Alert
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementNotVisibleException
from selenium.common.exceptions import ElementNotSelectableException, StaleElementReferenceException, NoSuchFrameException, UnexpectedAlertPresentException, NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.select import By
from selenium.webdriver.common.service import time
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common import keys, by
from selenium.webdriver.common.keys import Keys
from time import sleep

import pandas as pd
from pandas import ExcelWriter
import numpy as np
from scipy import stats


greens = ['#edf8fb','#ccece6','#99d8c9','#66c2a4','#41ae76','#238b45','#005824']
pinks = ['#f1eef6','#d4b9da','#c994c7','#df65b0','#e7298a','#ce1256','#91003f']
colorguide = {'< $30':'#000000',
'< $33': '#225ea8',
'< $36':'#1d91c0',
'< $40':'#41b6c4',
'< $45':'#7fcdbb',
'< $50':'#c7e9b4',$30
'>= $50':'#ffffcc'
}



if True:  
  allData = pd.read_csv('C:/atelier/rvu_prov.csv')
  aD = allData[allData['total_rvu_charges']>=20000]
  myStates = list(set( aD['state'].tolist()))
  keepers = []
  for thisState in myStates:
      myData = aD[aD['state']==thisState]
      tP = sum(myData['total_rvu_payments'].tolist())
      tR= sum(myData['total_tRVU'].tolist())
      keepers.append([thisState,tP/tR])
  k = pd.DataFrame(keepers,columns=['state','avg'])
  categs = []
  for a in k['avg'].tolist():
      if a<30:
          categs.append('< $30')
      elif a<33:
          categs.append('< $33')
      elif a<36:
          categs.append('< $36')
      elif a<40:
          categs.append('< $40')
      elif a<45:
          categs.append('< $45')
      elif a<50:
          categs.append('< $50')
      else:
          categs.append('>= $50')
  k['categ']= categs
  allData = k
  rawStatefile = open('C:/Users/Suzanne/Box/PCA Maps/States Plain SVG.txt',"r")
  rawStates = rawStatefile.read().decode('utf-8','ignore')
  pathSet = rawStates.split("<path")
  outputString = rawStates[0]
  print(outputString)
  for j in xrange(1,len(pathSet)):  #iterating through all the FIPS in the SVG
     thisPath = pathSet[j]
     print(thisPath)
     idThis = thisPath.find("id=")
     thisID = thisPath[idThis+4:idThis+6]
     print(thisID)
     rowMatch = allData[(allData['state'] == thisID)]  #finds the county with that FIPS in imported data
     if (len(rowMatch) > 0):
         rowMatch.index = range(len(rowMatch))
         peds =str( rowMatch.loc[0,'categ'])
         col = colorguide[peds]
         thisPath = thisPath.replace("fill:#aaaaaa","fill:"+col)

     outputString += "<path" + thisPath
  legend = ''
  text_file = open('C:/atelier/RVU_by_state.svg',"w")
  header = open('C:/atelier/StateMapHeader.txt','r')
  h = header.read()
  text_file.write(h)
  text_file.write(outputString[1:-7])

  text_file.write('</svg>')
  text_file.close() 
  header.close()


