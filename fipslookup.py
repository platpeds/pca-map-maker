from selenium import webdriver
from selenium import common
from datetime import datetime
import csv
import re
import math
import linecache
from selenium.common import exceptions
from selenium.webdriver.common.alert import Alert
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementNotVisibleException
from selenium.common.exceptions import ElementNotSelectableException, StaleElementReferenceException, NoSuchFrameException, UnexpectedAlertPresentException, NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.select import By
from selenium.webdriver.common.service import time
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common import keys, by
from selenium.webdriver.common.keys import Keys
from time import sleep
from bs4 import BeautifulSoup
import pandas as pd
from pandas import ExcelWriter
import numpy

from collections import Counter

if True:
   zipData = pd.read_excel('C:/VSHPScraper/Zips for Lookup.xlsx')
   driver = webdriver.Chrome(ChromePath)
   webpage = 'https://www.zipinfo.com/search/zipcode.htm'
   driver.get(webpage)
   xpathcheck = '//input[@name="cnty"]'
   driver.find_element_by_xpath(xpathcheck).click()
   xpathzip = '//input[@name="zip"]'
   xpathgo = '//input[@name="Go"]'
   fips=[]
   for i in xrange(0,len(zipData.index)):
       thisZip = str(zipData.loc[i,'zips']).zfill(5)
       driver.find_element_by_xpath(xpathzip).send_keys(thisZip)
       driver.find_element_by_xpath(xpathgo).click()
       html = driver.page_source
       startHere = html.find("</th></tr>")
       endHere = html.find("</tr>",startHere+8)
       bitTo = (html[startHere:endHere])
       fip = (bitTo[len(bitTo)-10:len(bitTo)-5])
       print(fip)
       fips.append(fip)
       driver.back()
       driver.find_element_by_xpath(xpathzip).send_keys(Keys.CONTROL+'a')
       driver.find_element_by_xpath(xpathzip).send_keys(Keys.DELETE)
   zipData['fips'] = fips
   zipData.to_excel('C:/VSHPScraper/FipsFound.xlsx') # zip to FIPS lookups
